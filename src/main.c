/*   Copyright (C) 2016 Martin Jääger
 *
 *   This file is part of I237_Riistvaralahedane_programmeerimine.
 *
 *   I237_Riistvaralahedane_programmeerimine is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   I237_Riistvaralahedane_programmeerimine is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with I237_Riistvaralahedane_programmeerimine.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include "uart-wrapper.h"
#include "print_helper.h"
#include "hmi_msg.h"
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "cli_microrl.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "rfid.h"

#define BAUD 9600

volatile uint32_t time_counter;
static uint32_t latest_time = 0;
int exists_in_list;
int finished;

// Create  microrl object  and pointer on  it
microrl_t rl;
microrl_t *prl = &rl;

static inline void init_system_clock(void)
{
    TCCR5A = 0; // Clear control register A
    TCCR5B = 0; // Clear control register B
    TCCR5B |= _BV(WGM52) | _BV(CS52); // CTC and fCPU/256
    OCR5A = 62549; // 1 s
    TIMSK5 |= _BV(OCIE5A); // Output Compare A Match Interrupt Enable
}


static inline uint32_t time(void)
{
    uint32_t now_time;
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        now_time = time_counter;
    }
    return now_time;
}


static inline void init_rfid_reader(void)
{
    MFRC522_init();
    PCD_Init();
}


static inline void print_startup_info()
{
    fprintf_P(stdout, PSTR(STUD_NAME));
    fputc('\n', stdout); // Add a new line to the uart printout
    lcd_puts_P(PSTR(STUD_NAME));
}


static inline void hw_initialize()
{
    DDRA |= _BV(DDA3);// pin 25 to output
    DDRA |= _BV(DDA1);// pin 23 to output
    uart0_init(UART_BAUD_SELECT(BAUD, F_CPU));
    uart3_init(UART_BAUD_SELECT(BAUD, F_CPU));
    init_system_clock();
    sei(); //Interrupts enabled
    stdout = stdin = &uart0_io;
    stderr = &uart3_out;
    lcd_init();
    lcd_clrscr();
    init_rfid_reader();
    print_startup_info();
    // Call init with ptr to microrl instance and print callback
    microrl_init (prl, uart0_puts);
    microrl_set_execute_callback (prl, cli_execute);// Set callback for execute
}


static inline void print_version_info()
{
    fprintf_P(stderr, PSTR(VER_FW),
              PSTR(GIT_DESCR), PSTR(__DATE__), PSTR(__TIME__));
    fprintf_P(stderr, PSTR(VER_LIBC), PSTR(__AVR_LIBC_VERSION_STRING__),
              PSTR(__VERSION__));
}


static inline void heartbeat()
{
    uint32_t time_now = time();

    if ((latest_time - time_now) > 0) {
        // toggle led on arduino pin 25
        PORTA ^= _BV(PORTA3);
        fprintf_P(stderr, PSTR(UPTIME "\n"), time_now);
    }

    latest_time = time_now;
}


static inline void card_handle()
{
    static uint32_t count_from_enter;
    static uint32_t count_from_rejected;

    if (PICC_IsNewCardPresent()) {
        int count = 0;

        if (head != NULL) {
            card_t *current;
            current = head;

            while (current != NULL) {
                Uid card_uid;
                Uid *uid_ptra = &card_uid;
                PICC_ReadCardSerial(uid_ptra);

                if (!memcmp(card_uid.uidByte, current->uid, card_uid.size)) {
                    exists_in_list = 1;
                    PORTA |= _BV(PORTA1);
                    count_from_enter = time_counter;
                    lcd_clrscr();
                    lcd_puts_P(PSTR(STUD_NAME));
                    lcd_goto(0x40);
                    lcd_puts(current->name);
                    finished = 0;
                    count = 1;
                }

                current = current->next;
            }
        }

        if (head == NULL || count == 0) {
            exists_in_list = 0;
            PORTA &= ~_BV(PORTA1);
            count_from_rejected = time_counter;
            lcd_clrscr();
            lcd_puts_P(PSTR(STUD_NAME));
            lcd_goto(0x40);
            lcd_puts_P(PSTR(NO_ACCESS));
            finished = 0;
        }

        exists_in_list = count;
    }

    if (exists_in_list == 1 && (time_counter - count_from_enter) < 3) {
        PORTA |= _BV(PORTA1);
    } else {
        PORTA &= ~_BV(PORTA1);
    }

    if (((time_counter - count_from_rejected) >= 7) &&
            ((time_counter - count_from_enter) >= 7) && finished == 0) {
        lcd_clrscr();
        lcd_puts_P(PSTR(STUD_NAME));
        finished = 1;
    }
}


int main (void)
{
    hw_initialize();
    print_version_info();
    //print_startup_info();

    while (1) {
        heartbeat();
        //  CLI commands are handled in cli_execute()
        microrl_insert_char (prl, cli_get_char());
        card_handle();
    }
}


/* System clock ISR */
ISR(TIMER5_COMPA_vect)
{
    time_counter++;
}

