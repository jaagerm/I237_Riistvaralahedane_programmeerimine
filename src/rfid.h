/*   Copyright (C) 2016 Martin Jääger
 *
 *   This file is part of I237_Riistvaralahedane_programmeerimine.
 *
 *   I237_Riistvaralahedane_programmeerimine is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   I237_Riistvaralahedane_programmeerimine is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with I237_Riistvaralahedane_programmeerimine.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RFID_H_
#define _RFID_H_

typedef struct card {
    uint8_t uid_size;
    uint8_t uid[10];
    char *name;
    struct card *next;
} card_t;

extern card_t *head;
extern void rfid_list_of_cards(void);
extern void rfid_remove_card_using_name(const char *name);

#endif /* _RFID_H_ */
