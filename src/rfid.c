/*   Copyright (C) 2016 Martin Jääger
 *
 *   This file is part of I237_Riistvaralahedane_programmeerimine.
 *
 *   I237_Riistvaralahedane_programmeerimine is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   I237_Riistvaralahedane_programmeerimine is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with I237_Riistvaralahedane_programmeerimine.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <avr/pgmspace.h>
#include "rfid.h"
#include "hmi_msg.h"
#include "../lib/matejx_avr_lib/mfrc522.h"

card_t *head = NULL;

void rfid_list_of_cards(void)
{
    if (head == NULL) {
        printf_P(PSTR(NO_CARDS_ADDED));
    } else {
        int count = 1;
        card_t *current;
        current = head;

        while (current->next != NULL) {
            printf("%d. ", count);
            uint8_t *uid = current->uid;

            for (byte i = 0; i < current->uid_size; i++) {
                printf("%02X", uid[i]);
            }

            printf(" %s", current->name);
            current = current->next;
            count++;
            printf_P(PSTR("\n"));
        }

        printf("%d. ", count);
        uint8_t *uid = current->uid;

        for (byte i = 0; i < current->uid_size; i++) {
            printf("%02X", uid[i]);
        }

        printf(" %s", current->name);
    }

    printf_P(PSTR("\n"));
}


void rfid_remove_card_using_name(const char *curr_name)
{
    card_t *current;
    card_t *previous;
    current = head;
    previous = head;
    int is_listed = 0;

    while (current != NULL) {
        if (current == head && current->next == NULL &&
                !memcmp(current->name, curr_name, (strlen(current->name) + 1))) {
            head = NULL;
            free(current->name);
            free(current);
            current = NULL;
            previous = NULL;
            is_listed = 1;
        } else if (current == head && current->next != NULL &&
                   !memcmp(current->name, curr_name, (strlen(current->name) + 1))) {
            head = current-> next;
            free(current->name);
            free(current);
            current = head;
            previous = head;
            is_listed = 1;
        } else if (current->next != NULL &&
                   !memcmp(current->name, curr_name, (strlen(current->name) + 1))) {
            previous->next = current->next;
            free(current->name);
            free(current);
            current = previous->next;
            is_listed = 1;
        } else if (current->next == NULL  &&
                   !memcmp(current->name, curr_name, (strlen(current->name) + 1))) {
            previous->next = NULL;
            free(current->name);
            free(current);
            current = NULL;
            is_listed = 1;
        } else if (current->next != NULL) {
            previous = current;
            current = current->next;
        } else {
            previous = current;
            current = NULL;
        }
    }

    if (is_listed == 0) {
        putc('\n', stdout);
        printf_P(PSTR(CARD_NOT_FOUND));
    }

    printf_P(PSTR("\n"));
}
