/*   Copyright (C) 2016 Martin Jääger
 *
 *   This file is part of I237_Riistvaralahedane_programmeerimine.
 *
 *   I237_Riistvaralahedane_programmeerimine is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   I237_Riistvaralahedane_programmeerimine is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with I237_Riistvaralahedane_programmeerimine.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _HMI_MSG_H_
#define _HMI_MSG_H_

#define VER_FW "Version: %S built on: %S %S\n"
#define VER_LIBC "avr-libc version: %S avr-gcc version: %S\n"
#define GET_MONTH_LETTER "Enter Month name first letter >"
#define STUD_NAME "Martin Jääger"
#define UPTIME "Uptime: %lu s"

#define HELP_CMD "help"
#define HELP_HELP "Get help"
#define VER_CMD "version"
#define VER_HELP "Print FW version"
#define ASCII_CMD "ascii"
#define ASCII_HELP "print ASCII tables"
#define MONTH_CMD "month"
#define MONTH_HELP "Find matching month from lookup list. Usage: month <string>"

#define CLI_IMP_CMDS "Implemented commands:"
#define CLI_NO_CMD "Command not implemented.\n Use <help> to get help."
#define CLI_ARGS_AMOUNT "To few or to many arguments for this command\nUse <help>"

#define READ_CMD "read"
#define READ_HELP "Read UID info from RFID card"
#define ADD_CMD "add"
#define ADD_HELP "Add new card to memory. Use syntax: add <username>"
#define REMOVE_CMD "remove"
#define REMOVE_HELP "Remove a card from the memory. Use syntax: remove <username>"
#define LIST_CMD "list"
#define LIST_HELP "List all valid cards"
#define MEM_CMD "mem"
#define MEM_HELP "Display memory statistics"
#define NO_ACCESS "ACCESS DENIED!"

#define NO_MEMORY_LEFT "No memory left."
#define NO_CARDS_ADDED "No cards added to list"
#define CARD_NOT_FOUND "Card was not found"
#define CAN_NOT_DETECT_CARD "No card detected!"
#define CARD_SELECTED "Card selected!"
#define UID_SIZE "UID size: 0x%02X"
#define UID_SAK "UID sak: 0x%02X"
#define CARD_UID "Card UID: "

extern PGM_P const months[];

extern const char help_cmd[];
extern const char help_help[];
extern const char ver_cmd[];
extern const char ver_help[];
extern const char ascii_cmd[];
extern const char ascii_help[];
extern const char month_cmd[];
extern const char month_help[];
extern const char read_cmd[];
extern const char read_help[];
extern const char add_cmd[];
extern const char add_help[];
extern const char remove_cmd[];
extern const char remove_help[];
extern const char list_cmd[];
extern const char list_help[];
extern const char mem_cmd[];
extern const char mem_help[];

#endif /* _HMI_MSG_H endif */
