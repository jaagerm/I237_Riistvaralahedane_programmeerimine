/*   Copyright (C) 2016 Martin Jääger
 *
 *   This file is part of I237_Riistvaralahedane_programmeerimine.
 *
 *   I237_Riistvaralahedane_programmeerimine is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   I237_Riistvaralahedane_programmeerimine is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with I237_Riistvaralahedane_programmeerimine.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <avr/pgmspace.h>
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "hmi_msg.h"
#include "print_helper.h"
#include "cli_microrl.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "rfid.h"
#include "../lib/andy_brown_memdebug/memdebug.h"

#define NUM_ELEMS(x)        (sizeof(x) / sizeof((x)[0]))
#define UART_STATUS_MASK    0x00FF

void cli_print_help(const char *const *argv);
void cli_print_ver(const char *const *argv);
void cli_print_ascii_tbls(const char *const *argv);
void cli_handle_month(const char *const *argv);
void cli_print_cmd_error(void);
void cli_print_cmd_arg_error(void);
void cli_rfid_read(const char *const *argv);
void cli_rfid_add(const char *const *argv);
void cli_rfid_remove(const char *const *argv);
void cli_rfid_list(const char *const *argv);
void cli_mem_stat(const char *const *argv);


typedef struct cli_cmd {
    PGM_P cmd;
    PGM_P help;
    void (*func_p)();
    const uint8_t func_argc;
} cli_cmd_t;


const cli_cmd_t cli_cmds[] = {
    {help_cmd, help_help, cli_print_help, 0},
    {ver_cmd, ver_help, cli_print_ver, 0},
    {ascii_cmd, ascii_help, cli_print_ascii_tbls, 0},
    {month_cmd, month_help, cli_handle_month, 1},
    {read_cmd, read_help, cli_rfid_read, 0},
    {add_cmd, add_help, cli_rfid_add, 1},
    {remove_cmd, remove_help, cli_rfid_remove, 1},
    {list_cmd, list_help, cli_rfid_list, 0},
    {mem_cmd, mem_help, cli_mem_stat, 0}
};


void cli_print(const char *str)
{
    printf("%s", str);
}


char cli_get_char(void)
{
    if (uart0_peek() != UART_NO_DATA) {
        return uart0_getc() & UART_STATUS_MASK;
    } else {
        return 0x00;
    }
}


void cli_print_help(const char *const *argv)
{
    (void) argv;
    putc('\n', stdout);
    printf_P(PSTR(CLI_IMP_CMDS "\n"));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        printf_P(cli_cmds[i].cmd);
        printf_P(PSTR(" : "));
        printf_P(cli_cmds[i].help);
        putc('\n', stdout);
    }
}


void cli_print_ver(const char *const *argv)
{
    (void) argv;
    putc('\n', stdout);
    fprintf_P(stdout, PSTR(VER_FW),
              PSTR(GIT_DESCR), PSTR(__DATE__), PSTR(__TIME__));
    fprintf_P(stdout, PSTR(VER_LIBC), PSTR(__AVR_LIBC_VERSION_STRING__),
              PSTR(__VERSION__));
}


void cli_print_ascii_tbls(const char *const *argv)
{
    (void) argv;
    putc('\n', stdout);
    print_ascii_tbl (stdout);
    unsigned char ascii[128];

    for (unsigned char i = 0; i < sizeof(ascii); i++) {
        ascii[i] = i;
    }

    print_for_human(stdout, ascii, sizeof(ascii));
}


void cli_handle_month(const char *const *argv)
{
    putc('\n', stdout);
    lcd_goto(0x40);/* Move to next line beginning */
    char available_to_print = 16;

    for (int i = 0; i < 6; i++) {
        if (!strncmp_P(argv[1], (PGM_P)pgm_read_word(&months[i]), strlen(argv[1]))) {
            char printed_amount;
            printed_amount = fprintf_P(stdout, (PGM_P)pgm_read_word(&months[i]));
            fputc('\n', stdout);
            lcd_puts_P((PGM_P)pgm_read_word(&months[i]));
            lcd_putc(' ');
            available_to_print -= (printed_amount + 1);
        }
    }

    /* Clean second line */
    for (; available_to_print > -1; available_to_print--) {
        lcd_putc(' ');
    }
}


void cli_print_cmd_error(void)
{
    putc('\n', stdout);
    printf_P(PSTR(CLI_NO_CMD "\n"));
}


void cli_print_cmd_arg_error(void)
{
    putc('\n', stdout);
    printf_P(PSTR(CLI_ARGS_AMOUNT "\n"));
}


void cli_rfid_read(const char *const *argv)
{
    (void) argv;
    Uid uid;
    Uid *uid_ptr = &uid;

    if (PICC_IsNewCardPresent()) {
        putc('\n', stdout);
        printf(CARD_SELECTED "\n");
        PICC_ReadCardSerial(uid_ptr);
        printf_P(PSTR(UID_SIZE "\n"), uid.size);
        printf_P(PSTR(UID_SAK"\n"), uid.sak);
        printf_P(PSTR(CARD_UID));

        for (byte i = 0; i < uid.size; i++) {
            printf("%02X", uid.uidByte[i]);
        }

        putc('\n', stdout);
    } else {
        putc('\n', stdout);
        printf_P((PSTR(CAN_NOT_DETECT_CARD "\n")));
    }
}


void cli_rfid_add(const char *const *argv)
{
    int already_exists = 0;

    if (PICC_IsNewCardPresent()) {
        if (head != NULL) {
            card_t *current;
            current = head;
            int count = 1;

            while (current != NULL && already_exists == 0) {
                uint8_t *uid = current->uid;
                Uid card_uid;
                Uid *uid_ptra = &card_uid;
                PICC_ReadCardSerial(uid_ptra);

                if (!memcmp(current->name, argv[1], (strlen(current->name) + 1)) ||
                        !memcmp(card_uid.uidByte, current->uid, card_uid.size)) {
                    already_exists = 1;
                    printf("%d. ", count);

                    for (byte i = 0; i < current->uid_size; i++) {
                        printf("%02X", uid[i]);
                    }

                    printf(" %s", current->name);
                }

                current = current->next;
                count++;
            }
        }

        if (already_exists == 0) {
            card_t *new_card;
            Uid uid;
            Uid *uid_ptr = &uid;
            new_card = malloc(sizeof(card_t));

            if (!new_card) { //malloc nullpointer check
                printf_P(PSTR(NO_MEMORY_LEFT "\n"));
                return;
            }

            PICC_ReadCardSerial(uid_ptr);
            memcpy(&(new_card->uid_size), &(uid.size), 1);
            memcpy(new_card->uid, uid.uidByte, uid.size);
            new_card->name = malloc(strlen(argv[1]) + 1);

            if (new_card == NULL || new_card->uid == NULL || new_card->name == NULL) {
                printf_P(PSTR(NO_MEMORY_LEFT "\n"));
            }

            strcpy(new_card->name, argv[1]);
            new_card->next = NULL;

            if (head != NULL) {
                card_t *current;
                current = head;

                while (current->next != NULL) {
                    current = current->next;
                }

                current->next = new_card;
            } else {
                head = new_card;
            }
        }
    } else if (already_exists == 0) {
        putc('\n', stdout);
        printf_P(PSTR(CAN_NOT_DETECT_CARD));
    }

    printf_P(PSTR("\n"));
}


void cli_rfid_remove(const char *const *argv)
{
    (void) argv;
    rfid_remove_card_using_name(argv[1]);
    //putc('\n', stdout);
}


void cli_rfid_list(const char *const *argv)
{
    (void) argv;
    putc('\n', stdout);
    rfid_list_of_cards();
}


void cli_mem_stat(const char *const *argv)
{
    (void) argv;
    extern int __heap_start, *__brkval;
    int v;
    int space;
    static int prev_space;
    space = (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
    printf_P(PSTR("Heap statistics\n"));
    printf_P(PSTR("Used: %d\n"), getMemoryUsed());
    printf_P(PSTR("Free: %d\n"), getFreeMemory());
    printf_P(PSTR("\nSpace between stack and heap:\n"));
    printf_P(PSTR("Current %d\n"), space);
    printf_P(PSTR("Previous %d\n"), prev_space);
    printf_P(PSTR("Change %d\n"), space - prev_space);
    printf_P(PSTR("\nFreelist\n"));
    printf_P(PSTR("Freelist size: %d\n"), getFreeListSize());
    printf_P(PSTR("Blocks in freelist: %d\n"), getNumberOfBlocksInFreeList());
    printf_P(PSTR("Largest block in freelist: %d\n"), getLargestBlockInFreeList());
    printf_P(PSTR("Largest freelist block: %d\n"),
             getLargestAvailableMemoryBlock());
    printf_P(PSTR("Largest allocable block: %d\n"), getLargestNonFreeListBlock());
    prev_space = space;
}


int cli_execute(int argc, const char *const *argv)
{
    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        if (!strcmp_P(argv[0], cli_cmds[i].cmd)) {
            // Test do we have correct arguments to run command
            // Function arguments count shall be defined in struct
            if ((argc - 1) != cli_cmds[i].func_argc) {
                cli_print_cmd_arg_error();
                return 0;
            }

            // Hand argv over to function pointer,
            // cross fingers and hope that funcion handles it properly
            cli_cmds[i].func_p (argv);
            return 0;
        }
    }

    cli_print_cmd_error();
    return 0;
}
